﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cliente
{
    public partial class ClienteForm : Form
    {
        bool isConnected = false;
        TcpClient mTcpClient;
        byte[] mRx;

        public ClienteForm()
        {
            InitializeComponent();
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {

                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        private void ClienteForm_Load(object sender, EventArgs e)
        {
            txtIP.Text = GetLocalIPAddress();
            txtPorto.Text = "1025";
        }

        private void btnlistening_Click(object sender, EventArgs e)
        {

            if (!isConnected)
            {
                IPAddress ipaddr;
                int nporto;
                btnlistening.Text = "Disconnectar";
                if (!IPAddress.TryParse(txtIP.Text, out ipaddr))
                {
                    txtmsg.Text += "Endreço IP Inválido" + Environment.NewLine;
                    return;
                }

                if (!int.TryParse(txtPorto.Text, out nporto))
                {
                    txtmsg.Text += "Porto Inválido" + Environment.NewLine;
                    return;
                }

                if (nporto < 1024 || nporto > 65535)
                {
                    txtmsg.Text += "Porto Inválido" + Environment.NewLine;
                    return;
                }
                mTcpClient = new TcpClient();
                mTcpClient.BeginConnect(ipaddr, nporto, onCompleteConnect, mTcpClient);

                txtmsg.Text += "A estabelecer conexão " + ipaddr + " " + nporto.ToString() + Environment.NewLine;
                isConnected = true;

            }
            else
            {

                isConnected = false;
                txtmsg.Text += "Connecção desligada" + Environment.NewLine;
                btnlistening.Text = "Connectar";

            }
        }
        private void onCompleteConnect(IAsyncResult iar)
        {
            TcpClient tcpc;
            tcpc = (TcpClient)iar.AsyncState;
            try
            {
                tcpc.EndConnect(iar);
            }
            catch (Exception)
            {
                txtmsg.Invoke((MethodInvoker)delegate
                {
                    txtmsg.Text += "Ligação não estabelecida " + Environment.NewLine;
                    
                });
                return;
            }

            txtmsg.Invoke((MethodInvoker)delegate
            {
                txtmsg.Text += "Ligação estabelecida " + Environment.NewLine;
            });
            mRx = new byte[1460];
            tcpc.GetStream().BeginRead(mRx, 0, mRx.Length, OnCompleteRead, mTcpClient);


        }
        private void OnCompleteRead(IAsyncResult iar)
        {
            TcpClient tcpc;
            string strRecv = String.Empty;
            int nReadBytes = 0;
            tcpc = (TcpClient)iar.AsyncState;
            nReadBytes = tcpc.GetStream().EndRead(iar);
            strRecv = ASCIIEncoding.ASCII.GetString(mRx, 0, nReadBytes);

            txtmsg.Invoke((MethodInvoker)delegate
            {
                txtmsg.Text += strRecv;

            });
            mRx = new byte[1460];
            mTcpClient.GetStream().BeginRead(mRx, 0, mRx.Length, OnCompleteRead, mTcpClient);

        }

        private void OnCompleteWrite(IAsyncResult iar)
        {
            TcpClient tcpc = (TcpClient)iar.AsyncState;
            tcpc.GetStream().EndWrite(iar);

        }


        private void enviar()
        {
            byte[] sendTxt = new byte[1460];
            if (mTcpClient == null)
            {
                txtmsg.Text += "O cliente não está ligado" + Environment.NewLine;
                return;
            }


            if (!mTcpClient.Client.Connected)
            {
                txtmsg.Text += "Perdeu-se a ligação " + Environment.NewLine;
                return;
            }
            sendTxt = ASCIIEncoding.ASCII.GetBytes(textBox1.Text + Environment.NewLine);
            mTcpClient.GetStream().BeginWrite(sendTxt, 0, sendTxt.Length, OnCompleteWrite, mTcpClient);
            textBox1.Text = String.Empty;
        }
        private void txtmsg_TextChanged(object sender, EventArgs e)
        {
            txtmsg.SelectionStart = txtmsg.TextLength;
            txtmsg.ScrollToCaret();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            enviar();
        }

       

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                enviar();
            }
        }

       
    }
}
