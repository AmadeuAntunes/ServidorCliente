﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Servidor
{
    public partial class Form1 : Form
    {
        TcpListener mTcpListener;
        TcpClient mTcpClient;
        byte[] mRx;
        bool isConnected = false;
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                  
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
        public Form1()
        {
            InitializeComponent();
            txtIP.Text =GetLocalIPAddress();
            txtPorto.Text = "1025";
        }

        private void btnlistening_Click(object sender, EventArgs e)
        {

            if (!isConnected)
            {
                IPAddress ipaddr;
                int nporto;
                btnlistening.Text = "Disconnectar";
                if (!IPAddress.TryParse(txtIP.Text, out ipaddr))
                {
                    txtmsg.Text += "Endreço IP Inválido" + Environment.NewLine;
                    return;
                }

                if (!int.TryParse(txtPorto.Text, out nporto))
                {
                    txtmsg.Text += "Porto Inválido" + Environment.NewLine;
                    return;
                }

                if (nporto < 1024 || nporto > 65535)
                {
                    txtmsg.Text += "Porto Inválido" + Environment.NewLine;
                    return;
                }
                    mTcpListener = new TcpListener(ipaddr, nporto);
                    mTcpListener.Start();
                    txtmsg.Text += "Servidor à escuta " + ipaddr + " " + nporto.ToString() + Environment.NewLine;
                    isConnected = true;
                
                    mTcpListener.BeginAcceptTcpClient(onCompletAcceptClient, mTcpListener);

                }
            else
            {

                mTcpListener.Stop();
                isConnected = false;
                txtmsg.Text += "Connecção desligada" + Environment.NewLine;
                btnlistening.Text = "Connectar";

            }
            
         
          
        }

        private void onCompletAcceptClient(IAsyncResult iar)
        {
            TcpListener tcpl = (TcpListener)iar.AsyncState;
           
                mTcpClient = tcpl.EndAcceptTcpClient(iar);
                txtmsg.Invoke((MethodInvoker)delegate
                {
                    txtmsg.Text += "Cliente Aceite..." + Environment.NewLine +
                    "IP: " + ((IPEndPoint)mTcpClient.Client.RemoteEndPoint).Address + Environment.NewLine +
                    "Porto: " + ((IPEndPoint)mTcpClient.Client.RemoteEndPoint).Port + Environment.NewLine;

                });
                mRx = new byte[1460];
                mTcpClient.GetStream().BeginRead(mRx, 0, mRx.Length, OnCompleteRead, mTcpClient);
            
        }

        private void OnCompleteRead(IAsyncResult iar)
        {
            TcpClient tcpc;
            string strRecv = String.Empty;
            int nReadBytes = 0;
            tcpc = (TcpClient)iar.AsyncState;
            nReadBytes = tcpc.GetStream().EndRead(iar);
            strRecv = ASCIIEncoding.ASCII.GetString(mRx, 0, nReadBytes);

            txtmsg.Invoke((MethodInvoker)delegate
            {
                 txtmsg.Text += strRecv;
              
            });
            mRx = new byte[14620];
            mTcpClient.GetStream().BeginRead(mRx, 0, mRx.Length, OnCompleteRead, mTcpClient);
          
        }


        private void enviar()
        {
            byte[] sendTxt = new byte[1460];
            if (mTcpClient == null)
            {
                txtmsg.Text += "O cliente não está ligado" + Environment.NewLine;
                return;
            }


            if (!mTcpClient.Client.Connected)
            {
                txtmsg.Text += "Perdeu-se a ligação " + Environment.NewLine;
                return;
            }
            sendTxt = ASCIIEncoding.ASCII.GetBytes(textBox1.Text + Environment.NewLine);
            mTcpClient.GetStream().BeginWrite(sendTxt, 0, sendTxt.Length, OnCompleteWrite, mTcpClient);
            textBox1.Text = String.Empty;
        }
       
        private void txtmsg_TextChanged(object sender, EventArgs e)
        {
            txtmsg.SelectionStart = txtmsg.TextLength;
            txtmsg.ScrollToCaret();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            enviar();
        }

        private void OnCompleteWrite(IAsyncResult iar)
        {
            TcpClient tcpc = (TcpClient)iar.AsyncState;
            tcpc.GetStream().EndWrite(iar);

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
           if(e.KeyChar == (char)13)
           {
                enviar();
                MessageBox.Show("oi");
            }
        }
    }
  

}
